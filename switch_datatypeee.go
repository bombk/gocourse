package main

import "fmt"

func main() {
	var x interface{} = 10
	switch x.(type) {
	case int:
		fmt.Println("Int data")
	case string:
		fmt.Println("String data")
	case float64:
		fmt.Println("fload64 data")
	default:
		fmt.Println("none")

	}
}
