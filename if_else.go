package main

import "fmt"

func main() {

	for i := 1; i <= 10; i++ {

		if i%2 == 0 {
			fmt.Printf("Even value: %v \n", i)

		} else {
			fmt.Printf("odd value : %v \n", i)

		}
		if j := 0; j == 0 {
			fmt.Printf("true")
		}

		//used or operator
		if i > 1 || i < 5 {
			fmt.Printf("true")
		}

		//used and operator
		if i > 1 && i < 5 {
			fmt.Printf("true")
		}
	}
}
