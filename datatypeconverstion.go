package main

import (
	"fmt"
	"strconv"
)



func main(){

	var a int32=100
	var b int64 =int64(a)
	fmt.Printf("%v,%T",b,b)
	fmt.Println("")

	c :=2.5
	fmt.Printf("%v,%T",c,c)
	fmt.Println("")

	var str int = 65
	var d string =strconv.Itoa(str)

	fmt.Printf("%v,%T",d,d)
}