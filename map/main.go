package main

import "fmt"

func main() {
	fmt.Println("Maps in golang")

	languages := make(map[string]string)
	languages["JS"] = "javascript"
	languages["RB"] = "ruby"
	languages["py"] = "python"
	languages["c"] = "c"
	fmt.Println("list of all language", languages)
	fmt.Println("list of all language", languages["py"])

	delete(languages, "c")
	fmt.Println("list of all language", languages)

	//loops are intersting in golang

	//this is key value

	for key, value := range languages {

		// fmt.Println(key)
		// fmt.Println(value)

		fmt.Printf("for key %v,vlaue is %v\n", key, value)
	}
}
