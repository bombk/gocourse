package main

import "fmt"

func main() {

	hitesh := User{"Hitesh", "hitesh@mail.com", true, 23}

	hitesh.GetStatus()
	hitesh.NewMail()
	fmt.Println(hitesh)

}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}

func (u User) GetStatus() {
	fmt.Println("is User active: ", u.Status)

}

func (u User) NewMail() {

	u.Email = "test@go.dev" //not change original object
	fmt.Println("Email of this use is ", u.Email)

}
