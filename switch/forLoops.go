package main

import "fmt"

func main() {
	fmt.Println("welcome to loops in golang")

	days := []string{"sunday", "monday", "tuesday", "wednesday", "thursday"}
	fmt.Println(days)

	// for d := 0; d < len(days); d++ {
	// 	fmt.Println(days[d])
	// }

	for i := range days { //this is work for array and slice
		fmt.Println(days[i])
	}

	//for each loop
	for index, day := range days {
		fmt.Printf("index is %v and value is %v\n", index, day)
	}

	//similar like while loop
	rougueValue := 1
	for rougueValue < 10 {

		if rougueValue == 2 {
			goto lco
		}

		if rougueValue == 5 {
			break

		}
		fmt.Println("value is : ", rougueValue)
		rougueValue++
		continue

	}

	//goto statement
lco: ///coming form above rougeValue value
	fmt.Println("jumping at learncode online")
}
