package main

import "fmt"

func main() {

	greeterTwo()
	greeter()
	fmt.Println("welcome to function in golang")
	greeter()

	result := adder(4, 5)

	fmt.Println("Result is : ", result)

	proResult := proAdder(1, 2, 3, 4, 5, 6, 7, 7, 4)
	fmt.Println("Pro result is : ", proResult)

}

func greeter() {

	fmt.Println("Namaste from golang")

}

func greeterTwo() {
	fmt.Println("another method")

}

func adder(x int, y int) int {
	return (x + y)
}

//what ever method value is taken
func proAdder(values ...int) int {
	total := 0
	for _, val := range values {
		total += val
	}
	return total
}
