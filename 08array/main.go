package main

import "fmt"

func main() {
	fmt.Println("Welcome to array")

	var funitList [4]string
	funitList[0] = "Apple"
	funitList[1] = "tomato"
	funitList[3] = "peach"

	fmt.Println("Fruit list is ", funitList)
	fmt.Println("Fruit list is ", len(funitList))

	var vegList = [3]string{"potato", "beans", "mushoorm"}
	fmt.Println("Veg list is ", vegList)
	fmt.Println("Veg list is ", len(vegList))

}
