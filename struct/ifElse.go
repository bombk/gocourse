package main

import "fmt"

func main() {

	fmt.Println("if else in golang")

	loginCount := 10
	var result string

	if loginCount < 10 {
		result = "Regular user"
	} else if loginCount > 10 {
		result = "watch out"
	} else {
		result = "Exactly 10 login count"
	}

	fmt.Println(result)

	if 9%2 == 0 {
		fmt.Println("Number is even")
	} else {
		fmt.Println("number is odd")
	}

	if num := 11; num < 10 {
		fmt.Println("number is less then 10")
	} else {
		fmt.Println("num is not less then 10")
	}

	// if err!=nil{

	// }

}
