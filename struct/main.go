package main

import "fmt"

func main() {
	fmt.Println("Struct in golan")

	hitesh := User{"hitesh", "bom@go.dev", true, 20}

	fmt.Println(hitesh)
	fmt.Printf("hitesh details: %+v\n", hitesh)
	fmt.Printf("Name is %v and email is %v ", hitesh.Name, hitesh.Email)

}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}
