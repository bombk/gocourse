package main

import "fmt"

func main() {

	sum, sub, mul, div, mod := cals(5, 2)
	fmt.Println(sum)
	fmt.Println(sub)
	fmt.Println(mul)
	fmt.Println(div)
	fmt.Println(mod)
}

func cals(x int, y int) (int, int, int, int, int) {
	return (x + y), (x - y), (x * y), (x / y), (x % y)
}
