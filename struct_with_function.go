package main

import "fmt"

type Employee struct {
	Empid   int
	EmpName string
	Empdept string
	Empnum  []string
}

func main() {
	emp := Employee{1, "Bom", "it", []string{"0923840", "23423"}}
	emp.showEmpInfo()
}

func (emp Employee) showEmpInfo() {
	fmt.Println(emp.Empid)
	fmt.Println(emp.EmpName)
	fmt.Println(emp.Empdept)
	fmt.Println(emp.Empnum)

}
