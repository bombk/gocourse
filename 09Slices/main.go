package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Welcome to slices")

	var fruitList = []string{"Apple", "Tomato", "Peach"}

	fmt.Println(fruitList)

	fruitList = append(fruitList, "Mango", "banana")
	fmt.Println(fruitList)

	fmt.Printf("Type of fruitlist is %T\n", fruitList)

	fruitList = append(fruitList[1:])
	fmt.Println(fruitList)
	fruitList = append(fruitList[0:3])
	fmt.Println(fruitList)
	fruitList = append(fruitList[:2])
	fmt.Println(fruitList)

	highscore := make([]int, 4)

	highscore[0] = 234
	highscore[1] = 32
	highscore[2] = 234
	highscore[3] = 234
	//highscore[4] = 77       //not support

	highscore = append(highscore, 555, 11, 555, 222) //this support
	fmt.Println(highscore)

	sort.Ints(highscore)
	fmt.Println(highscore)
}
