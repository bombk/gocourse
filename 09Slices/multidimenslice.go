package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
)

type data struct {
	a [][]string
}

func main() {
	fmt.Println("2d slices")

	a := [][]string{{"ram", "home"}, {"hari", "gita"}, {"hari", "gita"}, {"hari", "gita"}}

	json_data, err := json.Marshal(a)

	if err != nil {
		log.Fatal(err)
	}
	print(bytes.NewBuffer(json_data))
}
