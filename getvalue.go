package main
import (
	"fmt"
	"net/http"
	"io/ioutil"
	)
	

func fetchResponse(url string) string{
	resp, _ := http.Get(url)	
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func main() {	
	resp := fetchResponse("http://someurl.com")
	fmt.Println(resp)
}