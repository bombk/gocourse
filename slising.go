//slising use array

package main
import "fmt"

func main()  {
	
	//var arr [4] int =[4]int{1,2,3,4,5}
	//var arr = [...]int{1,2,3,4}

	//slicing
	var arr []int=[]int{1,2,3,4} //array size is not given
	fmt.Println(arr)
	fmt.Println(len(arr))

	arr2 :=arr
	fmt.Println(arr2)

	arr[1] =200
	fmt.Println(arr)
	fmt.Println(arr2)
	fmt.Println("capacit",cap(arr2))

	//another way of define
	var slic []int=make([]int,2, 4)
	fmt.Println(slic)
	fmt.Println("length",len(slic))
	fmt.Println("capacity",cap(slic))

	//append value
	slic =append(slic,1000)
	fmt.Println("append slice")

}