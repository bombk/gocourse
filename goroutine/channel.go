package main

import (
	"fmt"
	"sync"
)

func main() {
	fmt.Println("Channel in golang")

	myCh := make(chan int, 4)
	wg := &sync.WaitGroup{}

	// fmt.Println(<-myCh)   //create deadlock
	// myCh <- 5

	wg.Add(2)
	go func(ch chan int, wg *sync.WaitGroup) {
		fmt.Println(<-myCh)
		fmt.Println(<-myCh)
		fmt.Println(<-myCh)

		fmt.Println(<-myCh) //output give 0
		fmt.Println(<-myCh)

		wg.Done()

	}(myCh, wg)
	go func(ch chan int, wg *sync.WaitGroup) {
		myCh <- 0
		// myCh <- 6
		// myCh <- 7
		close(myCh)

		wg.Done()

	}(myCh, wg)

	wg.Wait()

}
