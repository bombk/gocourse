package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

func main() {

	myRandNumber, _ := rand.Int(rand.Reader, big.NewInt(7)) //give 6 random number
	fmt.Println(myRandNumber)
}
