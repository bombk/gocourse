//interface is used for behavriables and methods

package main

import "fmt"

func main() {

	var obj Vechicle = Bike{
		Name:  "honda",
		Color: "black",
		Price: 2312,
	}
	obj.ShowDetails()
	fmt.Println(obj.ShowName())

}

type Vechicle interface {
	ShowDetails() //holds methods
	ShowName() string
}

type Bike struct {
	Name, Color string // holds behaviours
	Price       float64
}

func (bike Bike) ShowDetails() {
	fmt.Println("Bike color : ", bike.Color)
	fmt.Println("Bike price : ", bike.Price)
}

func (bike Bike) ShowName() string {
	return bike.Name
}
