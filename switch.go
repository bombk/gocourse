package main

import "fmt"

func main() {
	i := 4

	switch i {

	case 1, 5, 6:
		fmt.Println("Hi")
		break //used for scepe line of code
	case 2, 4:
		fmt.Println("hello")
		fallthrough // it allow to excute just below case if it is true
	case 3:
		fmt.Println("hello")

	default:
		fmt.Println("none")

	}

}
