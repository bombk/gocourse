package main

import "fmt"

func main() {
	f := isEven(5)
	fmt.Println(f)
}

func isEven(x int) bool {
	return x%2 == 0
}
