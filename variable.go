package main
import "fmt"

var Val1 int =500 //global  (Pascal Case)

var myValu2 int =100 // package lavel (camelCase)


func main(){
	var a int  //declaration
	a=10       //initiatilzation
	fmt.Println(a)
	
	//2nd method
	var b int = 500 //declaration with initialization
	fmt.Println(b)  
	//3rd method
	var c=100     //declartion with initialization
	fmt.Println(c)

	//4th method
	d :=1111   //declaration with initialization
	fmt.Println(d)

}