package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("welcome to files in golang")
	content := "This need to go in files -learn code online"

	file, err := os.Create("./myfile.txt")
	checkNilErr(err)
	length, err := io.WriteString(file, content)
	checkNilErr(err)
	fmt.Println("lenght is : ", length)
	defer file.Close()
	readFile("./myfile.txt")
}

func readFile(filename string) {
	dataBytes, err := ioutil.ReadFile(filename)

	checkNilErr(err)
	fmt.Println("Text data inside file is \n", string(dataBytes))

}

func checkNilErr(err error) {

	if err != nil {
		panic(err)
	}

}
