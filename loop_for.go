package main

import "fmt"

func main() {
	i := 1
	for i <= 5 {
		fmt.Println("hello ") //it is just like do while loop
		i++

	}

	//another way
	for i := 1; i <= 5; i++ {
		fmt.Println("hi") //normal for loop
	}

}
