package main

import "fmt"

func main() {
	num := 4
	val(&num)

	fmt.Println(num)
}

func val(num *int) {
	*num = 40
	fmt.Println(*num)
}
