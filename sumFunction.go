package main

import "fmt"

func main() {
	sum(2, 5)
	sub(2, 5)
	div(2, 5)
	mul(2, 5)
	mod(2, 5)

}

func sum(x int, y int) {
	fmt.Println(x + y)
}
func sub(x int, y int) {
	fmt.Println(x - y)
}
func div(x int, y int) {
	fmt.Println(x / y)
}
func mul(x int, y int) {
	fmt.Println(x * y)
}

func mod(x int, y int) {
	fmt.Println(x % y)
}
