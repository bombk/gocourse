package main

import "fmt"

type Employee struct {
	EmpName int
}

func main() {
	var emp *Employee
	fmt.Println(emp)

	emp = new(Employee)
	fmt.Println(emp)  //point in memory location
	fmt.Println(*emp) //give value

}
