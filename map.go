package main
import "fmt"

func main()  {
	//empSal :=make(map[string]int) //declaration
	//empSal :=map[string]int{               //instilixation
	//"Neha":200000,
	//	"Raj" :25000,
	//	"Hari" :15000,

	//}

	empSal :=map[string]int{  // declartion with initlization 
		"Neha":200000,
		"Raj" :25000,
		"Hari" :15000,

	}

	emp :=empSal

	empSal["hari"] = 100  //add new key value

	empSal["Hari"] = 100000000  //update key value
	fmt.Println(empSal)
	fmt.Println(len(empSal))
	fmt.Println(empSal["Hari"])

	delete(empSal,"Raj")

	fmt.Println(emp)  //copy map in new map ...only point memory location

	//check key 
	ankit,flag :=empSal["ram"]
	fmt.Println(ankit,flag)  //present give true otherwise false

	

}