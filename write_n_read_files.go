package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("welcome to file creating in golang")
	content := "this need to go in a file locastion"

	file, err := os.Create("./myfiles.txt")

	if err != nil {
		panic(err)
	}

	lenghth, err := io.WriteString(file, content)
	if err != nil {
		panic(err)
	}

	fmt.Println("length is: ", lenghth)
	defer file.Close()

	readFile("./myfiles.txt")

}

func readFile(filename string) {
	databyte, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	fmt.Println("Test data inside the file is \n", databyte)
}
