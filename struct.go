package main

import "fmt"

type Employee struct {
	EmpId     int
	EmpName   string
	DeptName  string
	EmpMobile []string
}

func main() {

	emp := Employee{
		101,
		"ram",
		"IT",
		[]string{"98239109234", "23450943"},

		// //for random placement of value
		// EmpName: "Ravi",
		// EmpId: 1234,
		// EmpMobile: []string{"2134",{"21345"}},
	}

	fmt.Println(emp)

	emp2 := emp
	emp.EmpName = "Annya"
	fmt.Println(emp)
	fmt.Println(emp2)
}
