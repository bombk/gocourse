package main

import "fmt"

func main() {
	var x int = 90
	var b *int = &x

	fmt.Println(x)
	fmt.Println(b)  //give memory location
	fmt.Println(*b) //give value

}
