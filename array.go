package main

import "fmt"

func main()  {
	var arr [4]int =[4]int{1,2,3,4}
	fmt.Println(arr)

	arr1 :=[4]string{"ram","hari","git"}
	fmt.Println(arr1)

	var arr2 =[...]int{1,2,3,4,5,62,3,434,23,23}
	fmt.Println(arr2)
	fmt.Println(len(arr2))

	arr2[3] = 1000  //replace index value
	fmt.Println(arr2) 

	fmt.Println(arr2[3]) //print index value

	arr3 :=arr2
	fmt.Println(arr3)  //copy array

	fmt.Println(arr3[3:7])  //give value from index 3 to index 6
}