package main

import "fmt"

func main() {
	res := sum(3, 4, 5, 6, 4)
	fmt.Println(res)
}

func sum(vals ...int) int {
	sum := 0
	for _, n := range vals {
		sum += n
	}

	return sum
}
