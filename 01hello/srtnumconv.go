package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	welcome := "welcome to your pizza app"
	fmt.Println(welcome)

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the rating for our pizza")

	//commma ok || err ok syntax
	input, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("Thank for rating, ", input)
	fmt.Printf("type of varialbe is %T \n", input)

	numRating, err := strconv.ParseFloat(strings.TrimSpace(input), 64)
	if err != nil {
		fmt.Println(err)
		panic(err)
	} else {
		fmt.Println("added", numRating+1)
	}

	fmt.Print("Thank for rating, ", numRating)
	fmt.Printf("type of varialbe is %T \n", numRating)

}
