package main

import "fmt"

func main() {
	mynum := 23

	var ptr = &mynum
	fmt.Println("Value of & pointer ", ptr)
	fmt.Println("value of * pointer", *ptr)

	*ptr = *ptr + 2
	fmt.Println("New value is ", mynum)
}
