package main

import "fmt"

func main() {
	var username string = "bom"

	fmt.Println(username)

	fmt.Printf("variable is of type: %T \n", username)

	var isLoggedIn bool = true
	fmt.Println(isLoggedIn)
	fmt.Printf("variable type %T \n", isLoggedIn)

	var smallVal uint8 = 255
	fmt.Println(smallVal)
	fmt.Printf("variable is of type: %T\n", smallVal)

	var smallFloat float32 = 255.34255643245
	fmt.Println(smallFloat)
	fmt.Printf("Varialbe is of type: %T\n", smallFloat)

	//defaults value and some aliases

	var anotherVariable int
	fmt.Println(anotherVariable)
	fmt.Printf("variable is of type %T\n", anotherVariable)

	var website = "leancode.in"
	fmt.Println(website)
	fmt.Printf("variable type %T \n", website)

	numberOfurs := 321312.322345675432456432345
	fmt.Println(numberOfurs)
	fmt.Printf("variable type %T \n", numberOfurs)

}
