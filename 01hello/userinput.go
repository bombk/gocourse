package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	welcome := "Welcome to user input"
	fmt.Println(welcome)

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the rating for our pizza")

	//commma ok || err ok syntax
	input, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("Thank for rating, ", input)
	fmt.Printf("type of varialbe is %T \n", input)
}
