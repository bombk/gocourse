package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {

	var uname string
	fmt.Print("Enter username: ")
	fmt.Scanf("%s", &uname)

	url := "https://fttx-api.wlink.com.np/customer/" + uname + "/wifi"

	response, err := http.Get(url)

	if err != nil {
		panic(err)
	}

	defer response.Body.Close()

	content, err := ioutil.ReadAll(response.Body)

	if err != nil {
		panic(err)
	}
	//fmt.Println(string(content))
	var myData map[string]interface{}
	json.Unmarshal(content, &myData)
	//fmt.Printf("%#v\n", myData)
	dvid := myData["deviceId"]
	str := fmt.Sprint(dvid)
	bom := strings.Replace(str, "%", "%25", -1)
	fmt.Println(bom)

}
