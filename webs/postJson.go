package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	fmt.Println("This is post data in json")
	PostRequest()

}

func PostRequest() {
	const myurl = "http://127.0.0.1:5000/result"

	requestBody := strings.NewReader(`{
			"sn":"ALCLEB2D7FA2"
			"hostname":"google.com"
		}
		`)

	response, err := http.Post(myurl, "application/json", requestBody)

	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	content, _ := ioutil.ReadAll(response.Body)
	fmt.Println(string(content))
}
