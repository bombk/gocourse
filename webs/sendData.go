package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func main() {

	fmt.Println("Postring form data")
	PerformPostFormRequest()

}

func PerformPostFormRequest() {

	const myurl = "http://127.0.0.1:5000/result"

	//form Data
	data := url.Values{}
	data.Add("sn", "ALCLEB2D7FA2")
	data.Add("hostname", "google.com")

	response, err := http.PostForm(myurl, data)

	if err != nil {
		panic(err)
	}

	defer response.Body.Close()
	content, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(content))

}
