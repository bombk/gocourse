package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	uname := [...]string{"ashokkc_home",
		"mahendrashahi",
		"ommani_home",
		"prems_fahome",
		"test_fbdbs",
		"avirals_home",
		"pratyush_home"}
	for i := 0; i < len(uname); i++ {

		url := "https://fttx-api.wlink.com.np/customer/" + uname[i] + "/wifi"

		response, err := http.Get(url)

		if err != nil {
			panic(err)
		}

		defer response.Body.Close()

		content, err := ioutil.ReadAll(response.Body)

		if err != nil {
			panic(err)
		}
		//fmt.Println(string(content))
		var myData map[string]interface{}
		json.Unmarshal(content, &myData)
		//fmt.Printf("%#v\n", myData)
		dvid := myData["deviceId"]
		fmt.Println(dvid)

	}
}
