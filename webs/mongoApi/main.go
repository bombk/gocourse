package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/bombk/mongoapi/router"
)

func main() {
	fmt.Println("mongodb api")

	r := router.Router()
	fmt.Println("server is getting started")
	log.Fatal(http.ListenAndServe(":4000", r))

}
