package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type course struct {
	Name string `json:"name"` //give key naem work alike alies
	//dash removed or hide field
	Tags [][]string `json:"parameterValues,omitempty"` //give no nil value
}

func main() {
	fmt.Println("This is json")

	url := "http://gacs.wlink.com.np:8557/devices/6CC63B-G%252D140W%252DG-ALCLB399456D/tasks?connection_request"

	lcoCourse := []course{
		{"setParameterValues", [][]string{{"InternetGatewayDevice.ServerSelectionDiagnostics.DiagnosticsState", "Requested"}, {"InternetGatewayDevice.ServerSelectionDiagnostics.HostList", "8.8.8.8"}, {"InternetGatewayDevice.ServerSelectionDiagnostics.Interface", "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1"}}},
	}

	//finalJson, err := json.Marshal(lcoCourse)
	finalJson, err := json.Marshal(lcoCourse)

	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", finalJson)

	resp, err := http.Post(url, "application/json",
		bytes.NewBuffer(finalJson))

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(resp)

}
