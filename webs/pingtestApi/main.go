package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type params struct {
	name            string     `json:"name"`                      //give key naem work alike alies
	parameterValues [][]string `json:"parameterValues,omitempty"` //give no nil value
}

func main() {
	url := "http://gacs.wlink.com.np:8557/devices/6CC63B-G%252D140W%252DG-ALCLB399456D/tasks?connection_request"

	lcoCourse := []params{
		{"setParameterValues", [][]string{{"InternetGatewayDevice.ServerSelectionDiagnostics.DiagnosticsState", "Requested"}, {"InternetGatewayDevice.ServerSelectionDiagnostics.HostList", "8.8.8.8"}, {"InternetGatewayDevice.ServerSelectionDiagnostics.Interface", "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1"}}},
	}

	//package this data as json data

	fmt.Println("type %T", lcoCourse)

	//finalJson, err := json.Marshal(lcoCourse)
	finalJson, err := json.MarshalIndent(lcoCourse, "", "\t")

	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", finalJson)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post(url, "application/json",
		bytes.NewBuffer(finalJson))

	if err != nil {
		log.Fatal(err)
	}

	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)

	fmt.Println(res["json"])
}
