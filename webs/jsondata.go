package main

import (
	"encoding/json"
	"fmt"
)

type course struct {
	Name     string `json:"coursename"` //give key naem work alike alies
	Price    int
	Platform string   `json:"website"`
	Password string   `json:"-"`              //dash removed or hide field
	Tags     []string `json:"tags,omitempty"` //give no nil value
}

func main() {
	fmt.Println("This is json")
	EncodeJson()
	//DecodeJson()
}

func EncodeJson() {
	lcoCourse := []course{
		{"Reactjs", 299, "learncode", "abc@12", []string{"web", "js"}},
		{"Reactjs", 299, "learncode", "bde@12", []string{"web", "js"}},
		{"Reactjs", 299, "learncode", "fgs@12", []string{"web", "js"}},
		{"Reactjs", 299, "learncode", "dfs@12", nil},
	}

	//package this data as json data
	ram := lcoCourse[1:]

	fmt.Println(ram[1:])

	//finalJson, err := json.Marshal(lcoCourse)
	finalJson, err := json.MarshalIndent(lcoCourse, "", "\t")

	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", finalJson)
}

// func DecodeJson() {
// 	jsonDataFromWeb := []byte(`
// 	{
// 		"coursename": "Reactjs",
// 		"Price": 299,
// 		"website": "learncode",
// 		"tags": [
// 				"web",
// 				"js"
// 		]
// 	}
// 	`)

// 	// var lcoCourse course
// 	// checkvalid := json.Valid(jsonDataFromWeb)

// 	// if checkvalid {
// 	// 	fmt.Println("valid json")
// 	// 	json.Unmarshal(jsonDataFromWeb, &lcoCourse)
// 	// 	fmt.Printf("%#v\n", lcoCourse)
// 	// } else {
// 	// 	fmt.Println("json is not valid")
// 	// }

// 	var myOnlineData map[string]interface{}
// 	json.Unmarshal(jsonDataFromWeb, &myOnlineData)
// 	fmt.Printf("%#v\n", myOnlineData)

// 	for k, v := range myOnlineData {
// 		fmt.Printf("Key is %v and value is %v and type is %T\n", k, v)
// 	}

// }
