package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	fmt.Println("Welcome to web werb")
	PerformGetrequest()
}

func PerformGetrequest() {

	const myurl = "https://fttx-api.wlink.com.np/customer/bom_home/wifi"

	response, err := http.Get(myurl)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	fmt.Println("status code ", response.StatusCode)
	fmt.Println("content length is: ", response.ContentLength)

	content, _ := ioutil.ReadAll(response.Body)

	fmt.Println(string(content)) //geting data in string

	fmt.Println(content) //gettin data in bytes

	//another method using string library
	var responseString strings.Builder
	content1, _ := ioutil.ReadAll(response.Body)
	byteCount, _ := responseString.Write(content1)

	fmt.Println("Byte count is ", byteCount)
	fmt.Println()
	fmt.Println(responseString.String())

}
